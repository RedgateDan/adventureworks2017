CREATE TABLE [dbo].[T3]
(
[A] [int] NOT NULL,
[B] [int] NULL,
[C] [int] NULL,
[D] [int] NULL,
[E] [int] NULL,
[F] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[T3] ADD CONSTRAINT [A_PK] PRIMARY KEY CLUSTERED  ([A]) ON [PRIMARY]
GO
