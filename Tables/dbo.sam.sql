CREATE TABLE [dbo].[sam]
(
[samid] [int] NOT NULL,
[sam_name] [varchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[sam_surname] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[sam_desc] [char] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[sam_cost] [float] NULL
) ON [PRIMARY]
GO
