CREATE TABLE [Eddie].[Author]
(
[Author_ID] [int] NOT NULL IDENTITY(1, 1),
[AuthorName] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Eddie].[Author] ADD CONSTRAINT [prim_Author] PRIMARY KEY CLUSTERED  ([Author_ID]) ON [PRIMARY]
GO
